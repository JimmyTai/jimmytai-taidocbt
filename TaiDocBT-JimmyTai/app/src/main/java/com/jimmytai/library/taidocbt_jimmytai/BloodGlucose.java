package com.jimmytai.library.taidocbt_jimmytai;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by JimmyTai on 15/9/22.
 */
public class BloodGlucose extends VitalSignValue implements Serializable {

    private int glucose;
    private Date time;

    public BloodGlucose(int glucose, Date time) {
        this.glucose = glucose;
        this.time = time;
    }

    public int getGlucose() {
        return glucose;
    }

    public void setGlucose(int glucose) {
        this.glucose = glucose;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
