package com.jimmytai.library.taidocbt_jimmytai;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by JimmyTai on 15/9/22.
 */
public class BloodPressure extends VitalSignValue implements Serializable {

    private int systolic, diastolic, pulse;
    private Date time;

    public BloodPressure(int systolic, int diastolic, int pulse, Date time) {
        this.systolic = systolic;
        this.diastolic = diastolic;
        this.pulse = pulse;
        this.time = time;
    }

    public int getSystolic() {
        return systolic;
    }

    public void setSystolic(int systolic) {
        this.systolic = systolic;
    }

    public int getDiastolic() {
        return diastolic;
    }

    public void setDiastolic(int diastolic) {
        this.diastolic = diastolic;
    }

    public int getPulse() {
        return pulse;
    }

    public void setPulse(int pulse) {
        this.pulse = pulse;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
